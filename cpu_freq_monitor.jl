using GLMakie
using Dates

fig = Figure()
ax = Axis(fig[1, 1], title = "CPU freq.", ylabel = "CPU freq. [MHz]", xlabel = "time[s]", limits = (nothing, (0, nothing)))
# initial value
start = now()
points = Base.Sys.cpu_info() .|>
    (x -> x.speed) .|>
    (x -> Observable(Point2f[(0, x)]))
for (i, point) in enumerate(points)
    scatterlines!(ax, point, label = "#$(i)")
end
fig[1, 2] = Legend(fig, ax, "core")
@info points
display(fig)

# updating
max_points = 10
is_max_points = false
# for _ in 1:20 # for debug
while true
    @info is_max_points
    new_points = Base.Sys.cpu_info() .|>
        (x -> begin
            # @info x
            x.speed
        end) .|>
        (x -> begin
            # @info now() - start
            Point2f((now() - start).value / 1000, x)
        end)
    # @info new_points
    for (points, new_points) in zip(points, new_points)
        # @info points
        # @info new_points
        points[] = push!(points[], new_points)
        if is_max_points
            popfirst!(points[])
        end
    end
    # delete old points
    if length(points[1][]) > max_points
        global is_max_points = true
    end
    # points[] = push!(points[], new_points)
    display(fig)
    sleep(1)
end

